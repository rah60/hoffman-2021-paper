August 12, 2021

Contact:
Rachel Hoffman, rah60@duke.edu

General notes:
This repository contains scripts and other files needed to run figures from 2021 paper, in review. BAM files must be downloaded from SRA (https://www.ncbi.nlm.nih.gov/sra) with accession number PRJNA704995 to run all scripts. Merge and sample BAM files with scripts in "bash_scripts". Other datasets necessary to run scripts are contained in "csv", "macs2_files", "functions", "matrices" or the appropriate "FIG_" directory. 
