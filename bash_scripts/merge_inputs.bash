#!/bin/bash

#first, merge all replicates

#set directory to store merged bams
bam_file_dir="/data/home/rah60/merged_bam_files_2021/chip_input_sampled"

samtools merge ${bam_file_dir}/DM955_957_bwa_merged.bam /data/illumina_pipeline/aligned_experiments/DM955/DM955_sacCer3_2021-07-06-16-57_bwa.bam /data/illumina_pipeline/aligned_experiments/DM957/DM957_sacCer3_2021-07-06-17-07_bwa.bam

samtools merge ${bam_file_dir}/DM1214_1218_1465_bwa_merged.bam /data/home/rah60/merged_bam_files_2021/rfa1/DM1214_merged_sacCer3_bwa.bam /data/home/rah60/merged_bam_files_2021/rfa1/DM1218_merged_sacCer3_bwa.bam /data/illumina_pipeline/aligned_experiments/DM1465/DM1465_sacCer3_2021-07-07-23-38_bwa.bam

#enter directory with merged bam files
cd $bam_file_dir

#index all bam files
for f in *bwa_merged.bam
do
    samtools index $f
done

