#!/bin/bash

#filter MAPQ values in set of bam files

#specify BAM file dictory
bam_file_dir="/data/home/rah60/merged_bam_files_2021/mapq_filter"

#get files
bam_files=("/data/illumina_pipeline/aligned_experiments/DM1513/DM1513_sacCer3_2021-07-19-11-03_bwa.bam" "/data/illumina_pipeline/aligned_experiments/DM1514/DM1514_sacCer3_2021-07-19-11-54_bwa.bam" "/data/illumina_pipeline/aligned_experiments/DM1515/DM1515_sacCer3_2021-07-19-12-42_bwa.bam" "/data/illumina_pipeline/aligned_experiments/DM1516/DM1516_sacCer3_2021-07-19-13-30_bwa.bam" "/data/illumina_pipeline/aligned_experiments/DM1517/DM1517_sacCer3_2021-07-19-14-24_bwa.bam" "/data/illumina_pipeline/aligned_experiments/DM1518/DM1518_sacCer3_2021-07-19-15-00_bwa.bam" "/data/illumina_pipeline/aligned_experiments/DM1519/DM1519_sacCer3_2021-07-19-15-35_bwa.bam" "/data/illumina_pipeline/aligned_experiments/DM1520/DM1520_sacCer3_2021-07-19-16-28_bwa.bam" "/data/illumina_pipeline/aligned_experiments/DM1521/DM1521_sacCer3_2021-07-19-17-39_bwa.bam" "/data/illumina_pipeline/aligned_experiments/DM1522/DM1522_sacCer3_2021-07-19-18-43_bwa.bam" "/data/illumina_pipeline/aligned_experiments/DM1523/DM1523_sacCer3_2021-07-19-19-32_bwa.bam" "/data/illumina_pipeline/aligned_experiments/DM1524/DM1524_sacCer3_2021-07-19-20-09_bwa.bam")

labels=(DM1513 DM1514 DM1515 DM1516 DM1517 DM1518 DM1519 DM1520 DM1521 DM1522 DM1523 DM1524)

for(( f=0; f<${#bam_files[@]}; f++ ))
	do
	
	filter_name=${bam_file_dir}/${labels[$f]}_sacCer3_bwa_mapqfilter.bam
	
	samtools view -b -q 1 ${bam_files[$f]} > $filter_name
	
	samtools index $filter_name
	
	done
