#!/bin/bash

#first, merge all replicates

#set directory to store merged bams
bam_file_dir="/data/home/rah60/merged_bam_files_2021/mnase"

#cdc17-1,2-FRB files

samtools merge ${bam_file_dir}/DM916_DM1414_sacCer3_bwa.bam  /data/illumina_pipeline/aligned_experiments/DM916/DM916_sacCer3_2021-07-06-12-13_bwa.bam /data/illumina_pipeline/aligned_experiments/DM1414/DM1414_sacCer3_2021-07-08-03-32_bwa.bam

samtools merge ${bam_file_dir}/DM912_DM1418_sacCer3_bwa.bam  /data/illumina_pipeline/aligned_experiments/DM912/DM912_sacCer3_2021-07-07-11-03_bwa.bam /data/illumina_pipeline/aligned_experiments/DM1418/DM1418_sacCer3_2021-07-08-05-26_bwa.bam

samtools merge ${bam_file_dir}/DM913_DM1420_sacCer3_bwa.bam  /data/illumina_pipeline/aligned_experiments/DM913/DM913_sacCer3_2021-07-07-14-35_bwa.bam  /data/illumina_pipeline/aligned_experiments/DM1420/DM1420_sacCer3_2021-07-08-06-23_bwa.bam

samtools merge ${bam_file_dir}/DM917_DM1415_sacCer3_bwa.bam  /data/illumina_pipeline/aligned_experiments/DM917/DM917_sacCer3_2021-07-07-18-18_bwa.bam  /data/illumina_pipeline/aligned_experiments/DM1415/DM1415_sacCer3_2021-07-07-18-21_bwa.bam

samtools merge ${bam_file_dir}/DM919_DM1416_sacCer3_bwa.bam  /data/illumina_pipeline/aligned_experiments/DM919/DM919_sacCer3_2021-07-06-16-04_bwa.bam  /data/illumina_pipeline/aligned_experiments/DM1416/DM1416_sacCer3_2021-07-08-04-26_bwa.bam

samtools merge ${bam_file_dir}/DM908_DM1417_sacCer3_bwa.bam  /data/illumina_pipeline/aligned_experiments/DM908/DM908_sacCer3_2021-07-07-19-46_bwa.bam  /data/illumina_pipeline/aligned_experiments/DM1417/DM1417_sacCer3_2021-07-07-19-24_bwa.bam

samtools merge ${bam_file_dir}/DM909_DM1419_sacCer3_bwa.bam  /data/illumina_pipeline/aligned_experiments/DM909/DM909_sacCer3_2021-07-07-22-59_bwa.bam  /data/illumina_pipeline/aligned_experiments/DM1419/DM1419_sacCer3_2021-07-07-20-31_bwa.bam

#cdc17-1,2-FRB MRC1-FRB files
samtools merge ${bam_file_dir}/DM1319_DM1423_sacCer3_bwa.bam  /data/illumina_pipeline/aligned_experiments/DM1319/DM1319_sacCer3_2021-07-07-21-14_bwa.bam  /data/illumina_pipeline/aligned_experiments/DM1423/DM1423_sacCer3_2021-07-07-18-20_bwa.bam

samtools merge ${bam_file_dir}/DM1320_DM1425_sacCer3_bwa.bam  /data/illumina_pipeline/aligned_experiments/DM1320/DM1320_sacCer3_2021-07-07-22-50_bwa.bam  /data/illumina_pipeline/aligned_experiments/DM1425/DM1425_sacCer3_2021-07-07-19-31_bwa.bam

samtools merge ${bam_file_dir}/DM1321_DM1427_sacCer3_bwa.bam  /data/illumina_pipeline/aligned_experiments/DM1321/DM1321_sacCer3_2021-07-08-01-35_bwa.bam  /data/illumina_pipeline/aligned_experiments/DM1427/DM1427_sacCer3_2021-07-07-20-33_bwa.bam

samtools merge ${bam_file_dir}/DM1322_DM1429_sacCer3_bwa.bam  /data/illumina_pipeline/aligned_experiments/DM1322/DM1322_sacCer3_2021-07-08-03-56_bwa.bam  /data/illumina_pipeline/aligned_experiments/DM1429/DM1429_sacCer3_2021-07-07-21-37_bwa.bam

#enter directory with merged bam files
cd $bam_file_dir

#index all bam files
for f in *.bam
do
    samtools index $f
done