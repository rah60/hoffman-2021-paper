#!/bin/bash

#samples a set of chip files 

#specify BAM file dictory
bam_file_dir="/data/home/rah60/merged_bam_files_2021/chip_sampled"

#get files
bam_files=("/data/illumina_pipeline/aligned_experiments/DM949/DM949_sacCer3_2021-07-06-16-35_bwa.bam" "/data/illumina_pipeline/aligned_experiments/DM951/DM951_sacCer3_2021-07-06-16-39_bwa.bam" "/data/illumina_pipeline/aligned_experiments/DM952/DM952_sacCer3_2021-07-06-16-49_bwa.bam" "/data/home/rah60/merged_bam_files_2021/chip_input_sampled/DM955_957_bwa_merged.bam" "/data/home/rah60/merged_bam_files_2021/rfa1/DM1207_merged_sacCer3_bwa.bam" "/data/home/rah60/merged_bam_files_2021/rfa1/DM1211_merged_sacCer3_bwa.bam" "/data/home/rah60/merged_bam_files_2021/rfa1/DM1212_merged_sacCer3_bwa.bam" "/data/home/rah60/merged_bam_files_2021/chip_input_sampled/DM1214_1218_1465_bwa_merged.bam" "/data/illumina_pipeline/aligned_experiments/DM1371/DM1371_sacCer3_2021-07-07-18-17_bwa.bam" "/data/illumina_pipeline/aligned_experiments/DM1462/DM1462_sacCer3_2021-07-07-22-45_bwa.bam" "/data/illumina_pipeline/aligned_experiments/DM1464/DM1464_sacCer3_2021-07-07-23-07_bwa.bam")

labels=(DM949 DM951 DM952 DM955_57 DM1207 DM1211 DM1212 DM1214_1218_1465 DM1371 DM1462 DM1464)

readType=("SR" "SR" "SR" "SR" "PE" "PE" "PE" "PE" "PE" "PE" "PE")
#echo ${bam_files[@]}

declare -a readCount

#for number of files, iterate over bam file list to count read total
for(( f=0; f<${#bam_files[@]}; f++ ))
	do
	
	filter_name=${bam_file_dir}/${labels[$f]}_sacCer3_bwa_filter.bam
	
	if [ ${readType[$f]} == "PE" ]
	  then
	    samtools view -b -q 1 -f 2 ${bam_files[$f]} > $filter_name #proper pairs only
	
    	samtools index $filter_name
	
	   #get read count from each bam file
      readCount[$f]=$(samtools idxstats $filter_name | awk '!/chrXII/' |  awk '!/chrM/ {s+=$3} END {print s}')
	    readCount[$f]=$(echo print ${readCount[$f]}/2 | perl) #half read count for PE
	    
	else
	    samtools view -b -q 1 ${bam_files[$f]} > $filter_name
	
 	    samtools index $filter_name
	
	    #get read count from each bam file
      readCount[$f]=$(samtools idxstats $filter_name | awk '!/chrXII/' |  awk '!/chrM/ {s+=$3} END {print s}')
	    
	fi
	
	done

#find the min value, which is 
min_count=$(printf '%s\n' "${readCount[@]}" | sort -n | head -n 1)

#echo $min_count

#iterate over bam files again
for(( k=0; k<${#bam_files[@]}; k++ ))
  do 
  
  mapq_bam=${bam_file_dir}/${labels[$k]}_sacCer3_bwa_filter.bam
  
  #determine fraction of reads to sample
  sample_fraction=$(echo print $min_count/${readCount[$k]} | perl) 
  #echo $sample_fraction
  
  #name for new file
  new_file_name=${bam_file_dir}/${labels[$k]}_sacCer3_sampled_bwa.bam
  
  samtools view -s $sample_fraction -b $mapq_bam > $new_file_name
  
  samtools index $new_file_name
  
  done
