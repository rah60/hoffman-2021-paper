#!/bin/bash

#first, merge all replicates

#set directory to store merged bams
bam_file_dir="/data/home/rah60/merged_bam_files_2021/rfa1"

samtools merge ${bam_file_dir}/DM1207_merged_sacCer3_bwa.bam  /data/illumina_pipeline/aligned_experiments/DM1207/DM1207_sacCer3_2021-07-07-18-37_bwa.bam  /data/illumina_pipeline/aligned_experiments/DM1207_2/DM1207_sacCer3_2021-07-07-10-58_bwa.bam

samtools merge ${bam_file_dir}/DM1211_merged_sacCer3_bwa.bam  /data/illumina_pipeline/aligned_experiments/DM1211/DM1211_sacCer3_2021-07-07-18-50_bwa.bam /data/illumina_pipeline/aligned_experiments/DM1211_2/DM1211_sacCer3_2021-07-07-11-24_bwa.bam

samtools merge ${bam_file_dir}/DM1212_merged_sacCer3_bwa.bam  /data/illumina_pipeline/aligned_experiments/DM1212/DM1212_sacCer3_2021-07-07-19-00_bwa.bam /data/illumina_pipeline/aligned_experiments/DM1212_2/DM1212_sacCer3_2021-07-07-11-49_bwa.bam    

samtools merge ${bam_file_dir}/DM1214_merged_sacCer3_bwa.bam  /data/illumina_pipeline/aligned_experiments/DM1214/DM1214_sacCer3_2021-07-07-19-14_bwa.bam /data/illumina_pipeline/aligned_experiments/DM1214_2/DM1214_sacCer3_2021-07-07-12-27_bwa.bam

samtools merge ${bam_file_dir}/DM1218_merged_sacCer3_bwa.bam  /data/illumina_pipeline/aligned_experiments/DM1218/DM1218_sacCer3_2021-07-07-19-31_bwa.bam /data/illumina_pipeline/aligned_experiments/DM1218_2/DM1218_sacCer3_2021-07-07-13-21_bwa.bam

#enter directory with merged bam files
cd $bam_file_dir

#index all bam files
for f in *bwa.bam
do
    samtools index $f
done

