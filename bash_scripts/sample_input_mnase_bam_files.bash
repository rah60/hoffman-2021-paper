#!/bin/bash

#samples a set of chip files 

#specify BAM file dictory
bam_file_dir="/data/home/rah60/merged_bam_files_2021/mnase_input_sampled"

#get files
bam_files=("/data/illumina_pipeline/aligned_experiments/DM955/DM955_sacCer3_2021-07-06-16-57_bwa.bam" "/data/illumina_pipeline/aligned_experiments/DM958/DM958_sacCer3_2021-07-14-15-59_bwa.bam" "/data/illumina_pipeline/aligned_experiments/DM959/DM959_sacCer3_2021-07-14-16-05_bwa.bam" "/data/home/rah60/merged_bam_files_2021/rfa1/DM1214_merged_sacCer3_bwa.bam" "/data/home/rah60/merged_bam_files_2021/rfa1/DM1219_merged_sacCer3_bwa.bam" "/data/home/rah60/merged_bam_files_2021/rfa1/DM1220_merged_sacCer3_bwa.bam" "/data/home/rah60/merged_bam_files_2021/mnase/DM916_DM1414_sacCer3_bwa.bam" "/data/home/rah60/merged_bam_files_2021/mnase/DM909_DM1419_sacCer3_bwa.bam" "/data/home/rah60/merged_bam_files_2021/mnase/DM913_DM1420_sacCer3_bwa.bam")

labels=(DM955 DM958 DM959 DM1214 DM1219 DM1220 DM916_1414 DM909_1419 DM913_1420)

readType=("SR" "SR" "SR" "PE" "PE" "PE" "PE" "PE" "PE")
#echo ${bam_files[@]}

declare -a readCount

#for number of files, iterate over bam file list to count read total
for(( f=0; f<${#bam_files[@]}; f++ ))
	do
	
	filter_name=${bam_file_dir}/${labels[$f]}_sacCer3_bwa_filter_reviewresponse.bam
	
	if [ ${readType[$f]} == "PE" ]
	  then
	    samtools view -b -q 1 -f 2 ${bam_files[$f]} > $filter_name #proper pairs only
	
    	samtools index $filter_name
	
	   #get read count from each bam file
      readCount[$f]=$(samtools idxstats $filter_name | awk '!/chrXII/' |  awk '!/chrM/ {s+=$3} END {print s}')
	    readCount[$f]=$(echo print ${readCount[$f]}/2 | perl) #half read count for PE
	    
	else
	    samtools view -b -q 1 ${bam_files[$f]} > $filter_name
	
 	    samtools index $filter_name
	
	    #get read count from each bam file
      readCount[$f]=$(samtools idxstats $filter_name | awk '!/chrXII/' |  awk '!/chrM/ {s+=$3} END {print s}')
	    
	fi
	
	done

#find the min value, which is 
min_count=$(printf '%s\n' "${readCount[@]}" | sort -n | head -n 1)

#echo $min_count

#iterate over bam files again
for(( k=0; k<${#bam_files[@]}; k++ ))
  do 
  
  mapq_bam=${bam_file_dir}/${labels[$k]}_sacCer3_bwa_filter_reviewresponse.bam
  
  #determine fraction of reads to sample
  sample_fraction=$(echo print $min_count/${readCount[$k]} | perl) 
  #echo $sample_fraction
  
  #name for new file
  new_file_name=${bam_file_dir}/${labels[$k]}_sacCer3_sampled_bwa_reviewresponse.bam
  
  samtools view -s $sample_fraction -b $mapq_bam > $new_file_name
  
  samtools index $new_file_name
  
  done
