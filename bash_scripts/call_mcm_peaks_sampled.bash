#!/bin/bash

#commands to use MACS to find Mcm2-7 peaks, using input as a control

cd /data/home/rah60/FIGURES/macs2_files
#G1 data, single end, can use unsampled BAM files

macs2 callpeak -t /data/home/rah60/merged_bam_files_2021/chip_sampled/DM949_sacCer3_sampled_bwa.bam -g 12400000 -f BAM -n MCM_949_match_bwa_sampled -c /data/home/rah60/merged_bam_files_2021/chip_sampled/DM955_57_sacCer3_sampled_bwa.bam --call-summits

#output includes Excel file, .narrowPeak, .R file with model, and BED file. Use BED file for downstream analyses of peak positions

#40 min, 37+rap data

macs2 callpeak -t /data/home/rah60/merged_bam_files_2021/chip_sampled/DM951_sacCer3_sampled_bwa.bam -g 12400000 -f BAM -n MCM_951_match_bwa_sampled -c /data/home/rah60/merged_bam_files_2021/chip_sampled/DM955_57_sacCer3_sampled_bwa.bam --call-summits