#!/bin/bash

bam_file_dir="/data/home/rah60/merged_bam_files_2021/supplemental_chip_sampled"

samtools view -f 32 -b /data/home/rah60/merged_bam_files_2021/supplemental_chip_sampled/DM1214_1218_1465_sacCer3_sampled_bwa.bam | bedtools genomecov -bg -ibam stdin > /data/home/rah60/merged_bam_files_2021/supplemental_chip_sampled/DM1214_1218_1465_sampled_bwa_suppl.bdg
bedtools genomecov -bg -ibam /data/home/rah60/merged_bam_files_2021/supplemental_chip_sampled/DM955_57_sacCer3_sampled_bwa.bam > /data/home/rah60/merged_bam_files_2021/supplemental_chip_sampled/DM955_957_sampled_bwa_suppl.bdg

#get files
chip_files=("/data/home/rah60/merged_bam_files_2021/supplemental_chip_sampled/DM951_sacCer3_sampled_bwa.bam" "/data/home/rah60/merged_bam_files_2021/supplemental_chip_sampled/DM953_sacCer3_sampled_bwa.bam" "/data/home/rah60/merged_bam_files_2021/supplemental_chip_sampled/DM1371_sacCer3_sampled_bwa.bam" "/data/home/rah60/merged_bam_files_2021/supplemental_chip_sampled/DM1373_sacCer3_sampled_bwa.bam")

PE_input_file=("/data/home/rah60/merged_bam_files_2021/supplemental_chip_sampled/DM1214_1218_1465_sampled_bwa_suppl.bdg")

SR_input_file=("/data/home/rah60/merged_bam_files_2021/supplemental_chip_sampled/DM955_957_sampled_bwa_suppl.bdg")

chip_labels=(DM951 DM953 DM1371 DM1373)

readType=("SR" "SR" "PE" "PE")

for(( f=0; f<${#chip_files[@]}; f++ )) #for all chip files, convert to bed file and normalize to input with macs
	do
	
	new_file_name=${bam_file_dir}/${chip_labels[$f]}_sampled_bwa.bdg
	new_file_name2=${bam_file_dir}/${chip_labels[$f]}_inputnorm_sampled_bwa.bdg
  
  if [ ${readType[$f]} == "SR" ]
	  then
	    bedtools genomecov -bg -ibam ${chip_files[$f]} > $new_file_name
	    macs2 bdgcmp -t $new_file_name -c $SR_input_file -m subtract -o $new_file_name2
	fi
	
	if [ ${readType[$f]} == "PE" ]
	  then
	    samtools view -f 32 -b ${chip_files[$f]} | bedtools genomecov -bg -ibam stdin > $new_file_name
	    macs2 bdgcmp -t $new_file_name -c $PE_input_file -m subtract -o $new_file_name2
	fi
	
	done
	
