#!/bin/bash

#specify BAM file dictory
bam_file_dir="/data/home/rah60/merged_bam_files_2021/mnase"

#get file names
bam_files=($(ls ${bam_file_dir}/*bwa.bam))
#echo ${bam_files[@]}

declare -a readCount

#for number of files, iterate over bam file list to count read total
for(( f=0; f<${#bam_files[@]}; f++ ))
	do
	
	filter_name=${bam_files[$f]%.bam}_filtered.bam
	
	samtools view -b -q 1 -f 2 ${bam_files[$f]} > $filter_name #proper pairs and MAPQ > 1
	
	samtools index $filter_name

	#get read count from each bam file
  readCount[$f]=$(samtools idxstats $filter_name | awk '!/chrXII/' |  awk '!/chrM/ {s+=$3} END {print s}')
	
	done

#find the min value
min_count=$(printf '%s\n' "${readCount[@]}" | sort -n | head -n 1)

echo $min_count
echo ${readCount[@]}

#iterate over bam files again
for(( k=0; k<${#bam_files[@]}; k++ ))
  do

  mapq_bam=${bam_files[$k]%.bam}_filtered.bam

  #determine fraction of reads to sample
  sample_fraction=$(echo print $min_count/${readCount[$k]} | perl)
  #echo $sample_fraction

  #name for new file
  new_file_name=${bam_files[$k]%.bam}_sampled.bam

  samtools view -s $sample_fraction -b $mapq_bam > $new_file_name

  samtools index $new_file_name

  done
