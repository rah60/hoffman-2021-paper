#!/bin/bash

bam_file_dir="/data/home/rah60/merged_bam_files_2021/bdg_files"

bedtools genomecov -bg -ibam /data/home/rah60/merged_bam_files_2021/chip_sampled/DM955_57_sacCer3_sampled_bwa.bam > /data/home/rah60/merged_bam_files_2021/bdg_files/DM955_957_sampled_bwa.bdg
samtools view -f 32 -b /data/home/rah60/merged_bam_files_2021/chip_sampled/DM1214_1218_1465_sacCer3_sampled_bwa.bam | bedtools genomecov -bg -ibam stdin > /data/home/rah60/merged_bam_files_2021/bdg_files/DM1214_1218_1465_sampled_bwa.bdg

#get files
chip_files=("/data/home/rah60/merged_bam_files_2021/chip_sampled/DM949_sacCer3_sampled_bwa.bam" "/data/home/rah60/merged_bam_files_2021/chip_sampled/DM951_sacCer3_sampled_bwa.bam" "/data/home/rah60/merged_bam_files_2021/chip_sampled/DM952_sacCer3_sampled_bwa.bam" "/data/home/rah60/merged_bam_files_2021/chip_sampled/DM1207_sacCer3_sampled_bwa.bam" "/data/home/rah60/merged_bam_files_2021/chip_sampled/DM1211_sacCer3_sampled_bwa.bam" "/data/home/rah60/merged_bam_files_2021/chip_sampled/DM1212_sacCer3_sampled_bwa.bam" "/data/home/rah60/merged_bam_files_2021/chip_sampled/DM1371_sacCer3_sampled_bwa.bam" "/data/home/rah60/merged_bam_files_2021/chip_sampled/DM1462_sacCer3_sampled_bwa.bam" "/data/home/rah60/merged_bam_files_2021/chip_sampled/DM1464_sacCer3_sampled_bwa.bam")

PE_input_file=("/data/home/rah60/merged_bam_files_2021/bdg_files/DM1214_1218_1465_sampled_bwa.bdg")

SR_input_file=("/data/home/rah60/merged_bam_files_2021/bdg_files/DM955_957_sampled_bwa.bdg")

chip_labels=(DM949 DM951 DM952 DM1207 DM1211 DM1212 DM1371 DM1462 DM1464)

readType=("SR" "SR" "SR" "PE" "PE" "PE" "PE" "PE" "PE")

for(( f=0; f<${#chip_files[@]}; f++ )) #for all chip files, convert to bed file and normalize to input with macs
	do
	
	new_file_name=${bam_file_dir}/${chip_labels[$f]}_sampled_bwa.bdg
	new_file_name2=${bam_file_dir}/${chip_labels[$f]}_inputnorm_sampled_bwa.bdg
  
  if [ ${readType[$f]} == "SR" ]
	  then
	    bedtools genomecov -bg -ibam ${chip_files[$f]} > $new_file_name
	    macs2 bdgcmp -t $new_file_name -c $SR_input_file -m subtract -o $new_file_name2
	fi
	
	if [ ${readType[$f]} == "PE" ]
	  then
	    samtools view -f 32 -b ${chip_files[$f]} | bedtools genomecov -bg -ibam stdin > $new_file_name
	    macs2 bdgcmp -t $new_file_name -c $PE_input_file -m subtract -o $new_file_name2
	fi
	
	done
	
