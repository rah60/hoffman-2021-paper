#/usr/local/bin/R

# Clear the workspace
rm(list = ls())
graphics.off()

#plots Mcm2-7 ChIP data and AT kmers at origins, ordered by AT-rich region length

library(Rsamtools)
library(GenomicRanges)
library(readr)
library(viridis)
library(RColorBrewer)
library(stringr)

##call functions
source('/data/home/rah60/FIGURES/functions/plot_functions.R')

#setting up parameters 
plot_title <- c ("40 min Mcm, -Mrc1","AT content")
bin_size <- 1
initial_path <- "/data/home/rah60/FIGURES/FIG_6/"
pic_name <- "heatmap_239origins_mcm_mrc1_ATorder_bdg_bwa2.png"
window1 <- 3000 #total window to take around origin (bp)
kmer <- 3 #for AT kmer plot
chrEnds<-c("chrI"=230218, "chrII"=813184, "chrIII"=316620, "chrIV"=1531933, "chrV"=576874, "chrVI"=270161,"chrVII"=1090940, "chrVIII"=562643,"chrIX"=439888, "chrX"=745751, "chrXI"=666816, "chrXII"=1078177, "chrXIII"=924431,"chrXIV"=784333, "chrXV"=1091291, "chrXVI"=948066, "chrM"=0) #length of all 16 chr in order

#read in list of origins (239, good footprint) with AT region length
true_origins <- read_csv("~/FIGURES/FIG_6/239_origins_ATtracks_102920.csv") 

#make overall storage objects
all_data <- vector("list") #chip data storage
data <- matrix(0, nrow=nrow(true_origins), ncol=(window1/kmer+1) ) #kmer matrix
reordered_data <- vector("list")

mtx.save <- paste("/data/home/rah60/FIGURES/matrices/DM1371_bdgmtx_6000bpwindow.csv",sep="")
all_data[[1]] <- as.matrix(read.csv(mtx.save))
all_data[[1]] <- all_data[[1]]
all_data[[1]] <- all_data[[1]][,1500:4500] #narrow to 3000 bp window
true_origins$readcount <- rowSums(all_data[[1]])

  for(o in 1:nrow(true_origins)){
    
    if(is.na(true_origins[[o,17]])){next}
    
    window_start <- true_origins[[o,17]]-window1/2 #get ranges of positions centered on AT region
    window_end <- true_origins[[o,17]]+window1/2
    chr <- true_origins[[o,2]] #and chr for each origin
    if(window_start < 0){next}
    if(window_end > chrEnds[[chr]]){next}
    
    #get GR object in k bp bins
    kmer.gr <- GRanges(seqnames= chr, ranges = IRanges(start = seq(window_start, window_end, by=kmer) , width=kmer )) #by each bp
    
    seq_to_count <- getSeq(BSgenome.Scerevisiae.UCSC.sacCer3::Scerevisiae, kmer.gr) #get sequence
    if(true_origins[[o,4]] == "-"){ #flip origins on neg. strand
      seq_to_count <- rev(reverseComplement(seq_to_count))
    }
    split_kmers <- strsplit(toString(seq_to_count),",")
    
    #record kmers by AT or GC content
    for(k in 1:length(split_kmers[[1]])){
      str_len <- nchar(split_kmers[[1]][k])
      kmer_id <- str_sub(split_kmers[[1]][k],str_len-(kmer-1),str_len) #avoid the space at the front of some
  
      if(kmer_id=="AAA" | kmer_id=="TTT"){ #record AT kmers as 1 in matrix
        data[o,k] <- 1
      }
      if(kmer_id=="ATA" | kmer_id=="TAA"){
        data[o,k] <- 1
      }
      if(kmer_id=="ATT" | kmer_id=="TAT"){
        data[o,k] <- 1
      }
      if(kmer_id=="AAT" | kmer_id=="TTA"){
        data[o,k] <- 1
      }
    }#leave any kmers containing GCs as 0 in matrix
    
  } #end origin for
  


reordered_data[[1]] <- all_data[[1]][order(true_origins$at_track_length, decreasing = F),]
data2 <- data[order(true_origins$at_track_length, decreasing = F),]
true_origins <- true_origins[order(true_origins$at_track_length, decreasing = F),]

#set up timing & efficiency annotations
timing_color <- matrix(0, nrow=nrow(true_origins), ncol=1)
eff_color <- matrix(0, nrow=nrow(true_origins), ncol=1)

#get deciles of efficiencies
eff_list <- c()
for(t in 1:nrow(true_origins) ){
  eff1 <- true_origins[[t,9]]
  eff_list <- c(eff_list, eff1) 
}
deciles <- quantile(eff_list, prob=seq(0, 1, length=11))
eff_copy <- eff_list
eff_copy[which(eff_list < deciles[2])] <- 10
eff_copy[which(eff_list < deciles[3] & eff_list > deciles[2])] <- 20
eff_copy[which(eff_list < deciles[4] & eff_list > deciles[3])] <- 30
eff_copy[which(eff_list < deciles[5] & eff_list > deciles[4])] <- 40
eff_copy[which(eff_list < deciles[6] & eff_list > deciles[5])] <- 50
eff_copy[which(eff_list < deciles[7] & eff_list > deciles[6])] <- 60
eff_copy[which(eff_list < deciles[8] & eff_list > deciles[7])] <- 70
eff_copy[which(eff_list < deciles[9] & eff_list > deciles[8])] <- 80
eff_copy[which(eff_list < deciles[10] & eff_list > deciles[9])] <- 90
eff_copy[which(eff_list > deciles[10])] <- 100

for(i in 1:nrow(true_origins)){
  efficiency <- eff_copy[i] #use decile instead of actual efficiency
  eff_color[i,1] <- efficiency

  if(true_origins[i,8] == "early"){
    timing_color[i,] <- 1
  }else{
    timing_color[i,] <- 0
  }
}

#plotting parameters
col.v <- viridis(4)
col.v1 <- brewer.pal(10,"Paired")
v <- colorRampPalette(colors = c("navy","white"),bias=0.5)
col.v2 <- v(10)
x_window <- c(-window1/2, 0, window1/2)
x_spacing <- c(-ncol(reordered_data[[1]])/2, 0, ncol(reordered_data[[1]])/2)
x_spacing2 <- c(-ncol(data2)/2, 0, ncol(data2)/2)
fig_name_path <- paste(initial_path,pic_name, sep="")

#begin plotting
png(fig_name_path,width = 25, height = 30, units = "in", res = 100, bg = "transparent", type = "cairo-png") ##Open a device for plotting, using png(), bmp(), pdf() 

close.screen(all.screens = T)


#left, right, bottom, top
screen.m <- rbind( c(0.03, 0.1375, 0.05, 1), #timing
                   c(0.1475 ,0.255,0.05,1), #efficiency
                   c(0.275, 0.5, 0.05, 1), #chip data
                   c(0.52, 0.745, 0.05, 1), #AT 3mers
                   c(0.8, 0.92, 0.61, 0.88), #chip legend
                   c(0.765, 0.9, 0.30, 0.6), 
                   c(0.765, 0.9, 0.05, 0.32) 
)

split.screen(screen.m)

par(oma = c(4, 4, 4, 4)) 
  
  screen(1)
  par(mar = c(3,3,3,3), cex=2.5)  
  dens_dot_plot(timing_color, z_min = 0, z_max = 1, plot_title="Timing", x_axt = "n",y_axt = "n",lowCol = "gray",highCol = col.v[1])
  mtext(at=c(50,100,150), text=c(50,100,150),side=2,line=3,cex=3)
  axis(side=2, labels=F, tick=T, tck=-0.15) #y-axis
  
  screen(2) #efficiency
  par(mar = c(3,3,3,3), cex=2.5)  
  dens_dot_plot(eff_color, z_min = 10, z_max = 100, plot_title="Efficiency", x_axt = "n",y_axt = "n",lowCol = col.v2[10],medCol=col.v2[8], highCol = col.v2[1])
  axis(side=2, labels=F, tick=T, tck=-0.15)
  
  screen(3)
  par(mar = c(3,3,3,3), cex=2.5, cex.main=1.3)  
  dens_dot_plot(reordered_data[[1]],  z_min = 0, z_max = 20, plot_title=plot_title[[1]], x_axt = "n", y_axt = "n",highCol = col.v[2]) 
  #abline(h=h_lines, col="black", lwd=4)
  axis(side=1,labels=F, tick=T, tck=-0.04, at=x_spacing) #x-axis
  mtext(at=x_spacing, text=x_window, side=1, line=3, cex=3)
  axis(side=2, labels=F, tick=T, tck=-0.06) #y-axis
  
  screen(4)
  par(mar = c(3,3,3,3), cex=2.5, cex.main=1.3)  
  dens_dot_plot(data2,  z_min = -1, z_max = 1, plot_title=plot_title[[2]], x_axt = "n", y_axt = "n", lowCol="white",medCol = "white", highCol = col.v[1]) 
  #abline(h=h_lines, col="black", lwd=4)
  axis(side=1,labels=F, tick=T, tck=-0.04, at=x_spacing2) #x-axis
  mtext(at=x_spacing2, text=x_window, side=1, line=3, cex=3)
  axis(side=2, labels=F, tick=T, tck=-0.06) #y-axis

  screen(5)
  par(mar = c(3,3,3,3))  
  eff_color2 <- matrix(c(0,5,10,15,20), nrow=5, ncol=1)
  dens_dot_plot(eff_color2, z_min = 0, z_max = 20, plot_title="", x_axt = "n",y_axt = "n",lowCol = "white", highCol = col.v[2])
  mtext(at=c(1,2,3,4,5), text=c(0,5,10,15,">20"), side=2,cex=2.5)
  mtext(at=c(3), text="Mcm2-7 ChIP", side=2, line=3, cex=2.5)
  

mtext('Distance from origin (bp)', side = 1, outer = T, cex=4, at = 0.5, line=-1)
mtext('Origins of replication', side = 2, outer = T, cex=4, line=1)

dev.off()

