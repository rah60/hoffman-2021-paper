#/usr/local/bin/R

#Coverage across a chromosome in cdc17-1,2-FRB MRC1-FRB

# Clear the workspace
rm(list = ls())
graphics.off()

library(Rsamtools)
library(GenomicRanges)
library(readr)
library(viridis)

#files
file_path <- c("/data/home/rah60/merged_bam_files_2021/mnase/DM1319_DM1423_sacCer3_bwa_sampled",
               "/data/home/rah60/merged_bam_files_2021/mnase/DM1318_1428_sacCer3_bwa_sampled",
               "/data/home/rah60/merged_bam_files_2021/mnase/DM1322_DM1429_sacCer3_bwa_sampled")
number_of_files <- length(file_path)

true_origins <- read_csv("~/FIGURES/csv/oridb_acs_feature_file_jab-curated-798-sites_sacCer3.csv")
true_origins <- true_origins[true_origins$activation_time == "early",]
true_origins <- true_origins[true_origins$footprint_class == "g1_and_g2_footprint",]

# plot title 
plot_title =c("G1 phase", "24°C, 60 min","37°C + rapamycin, 60 min")

bin_size <- 500
bin_size_kb <- bin_size/1000
chrEnds <- c("chrI"=230218, "chrII"=813184, "chrIII"=316620, "chrIV"=1531933, "chrV"=576874, "chrVI"=270161,"chrVII"=1090940, "chrVIII"=562643,"chrIX"=439888, "chrX"=745751, "chrXI"=666816, "chrXII"=1078177, "chrXIII"=924431,"chrXIV"=784333, "chrXV"=1091291, "chrXVI"=948066, "chrM"=0) #length of all 16 chr in order
chooseChr <- "chrXV"
chr_origins <- true_origins[true_origins$chr == chooseChr,]

#setting up parameters
initial_path <- "/data/home/rah60/FIGURES/Supplemental/"

#make overall storage list
all_data <- vector("list")

for(f in 1:number_of_files){ 
  
  file_name.bam <- (paste(file_path[[f]],".bam", sep='')) 
  file_name.bam.bai <- paste(file_path[[f]],".bam.bai",sep='')

    window_start <- 1 #get ranges of positions
    window_end <- chrEnds[[chooseChr]]
    window <- window_end-window_start
    chr <- chooseChr #and chr for each origin
    
    #make into a GRanges object
    chr.gr <- GRanges(seqnames= chr, ranges = IRanges(start = window_start , end = window_end ))
    
    p <- ScanBamParam(what = c("rname", "strand", "pos", "isize"),which = chr.gr, flag = scanBamFlag(isMateMinusStrand = T,isProperPair = T))
    
    A_reads.l <- scanBam(file = file_name.bam,
                         index = file_name.bam.bai,
                         param = p)
    
    A_reads.gr <- GRanges(seqnames = A_reads.l[[1]]$rname,
                          ranges = IRanges(start = A_reads.l[[1]]$pos,
                                           width = A_reads.l[[1]]$isize))
    
    A_reads.gr <- A_reads.gr[which(width(A_reads.gr) > 140 & width(A_reads.gr) < 180)]
  
    or_cov <- coverage(A_reads.gr)
    chr_bins <- tileGenome(seqlengths=chrEnds, tilewidth = bin_size, cut.last.tile.in.chrom = T)
    bin_count <- binnedAverage(bins=chr_bins , numvar=or_cov, varname = "count")
    bins_subset <- keepSeqlevels(bin_count, chr, pruning.mode = "coarse")
    all_data[[f]] <- bins_subset$count
   
} #end file for


for(q in 1:number_of_files){
  non_replicating <- mean(all_data[[q]][1400:1600])

  all_data[[q]] <- all_data[[q]]/non_replicating #divide by non-rep region

} #end for
 
 filter_co <- 40

filtered <- as.vector(stats::filter(all_data[[1]], rep(1,filter_co) ,method="convolution"))/filter_co
filtered2 <- as.vector(stats::filter(all_data[[2]], rep(1,filter_co) ,method="convolution"))/filter_co
filtered3 <- as.vector(stats::filter(all_data[[3]], rep(1,filter_co) ,method="convolution"))/filter_co

color.v1 <- c(viridis(4))
color.v2 <- viridis(4, alpha=0.8)
x_seq <- seq(from=bin_size_kb,to=length(filtered)*bin_size_kb,by=bin_size_kb)

png(file = paste(initial_path,"coverage_mnasebwa_cdc17_mrc1_60",chooseChr,".png", sep=""), width = 20, height = 7, units = "in", res = 300)

par(cex=1.5)
plot(y=filtered2,x=x_seq, col=color.v1[[2]],type="l",ylab="Copy number", xlab=paste("Kb along ",chooseChr,sep=""), main=chooseChr,ylim=c(0,2.5),lwd=4,xaxs="i",xlim=c(20,1070))
legend(x = "topright", legend = c(plot_title[2],plot_title[3],plot_title[1]), col = color.v1[c(2,3,1)], lwd = 4, bty = "n")
lines(y=filtered3,x=x_seq, type="l", col=color.v2[[3]],lwd=4 )
lines(y=filtered,x=x_seq, type="l", col=color.v2[[1]], lwd=4 )
points(x=chr_origins$pos/1000, y=rep(0, times=nrow(chr_origins) ))
dev.off()

